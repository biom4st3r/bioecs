# BioECS
An Entity Component System for attatching data to ItemStacks In Minecraft.
Originally, my other project, Dynocaps, relied on [Cardinal Component](https://github.com/OnyxStudios/Cardinal-Components-API), but they transitioned to a new system specifically for Item Components that requires serialization and deserialization of all data on every access and store; this make my project far less performent so I made my own.

## Usage
1. Create a class that implements [Component](https://gitlab.com/biom4st3r-mods/bioecs/-/blob/default/src/main/java/biom4st3r/libs/bioecs/ecs/api/Component.java). If equality checking isn't enough for your use case there is also [SyncComponent](https://gitlab.com/biom4st3r-mods/bioecs/-/blob/default/src/main/java/biom4st3r/libs/bioecs/ecs/api/SyncComponent.java) which simply asks if you're component needs to update to the client. Dynocaps uses SyncComponents.

2. You need to create a ComponentKey to register your components
```java
public class SomeComponent implements Component {
	public static final ComponentKey<SomeComponent> KEY = ComponentKeyImpl.of(SomeComponent.class, SomeComponent::new, "bioecs:somecomponent");
}
```
This key will be used to interact with ItemStack in order to access your attached component

3. Register your ComponentKey to it's items IN YOUR **ModInitializer**
```java
@Override
public void onInitialize() {
	ComponentRegistry.register(Items.FISHING_ROD, MyComponent.KEY);
}
```


4. Using your component. Optional.EMPTY is returned if your Component isn't on the supplied ItemStack
```java
ItemStack someStack;
Optional<SomeComponent> component = SomeComponent.KEY.getComponent(someStack);
```

## Extra
### EntityDescription
This interface is applied to Item. It is for Objects that simply describe what components another object should have. For Example Item are used to create ItemStacks(an Item witha quantity and data) and therefor describe the ItemStack and what components it should have.

### ComponentProvider
This is applied to ItemStack. It is for Objects that hold instances of registered Components.
