package biom4st3r.libs.bioecs;

import java.util.Date;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;

import biom4st3r.libs.bioecs.ecs.api.Component;
import biom4st3r.libs.bioecs.ecs.api.ComponentKey;
import biom4st3r.libs.bioecs.ecs.api.ComponentProvider;
import biom4st3r.libs.bioecs.ecs.impl.ComponentKeyImpl;

public class SomeComponent implements Component {
	public static final ComponentKey<SomeComponent> KEY = ComponentKeyImpl.of(SomeComponent.class, SomeComponent::new, "bioecs:somecomponent");
	private long time = new Date().getTime();
	ItemStack is;

	private boolean synced = false;
	public SomeComponent() {
	}

	@Override
	public void serialize(NbtCompound ct) {
		ct.putLong("key", time);
	}

	@Override
	public void deserialize(NbtCompound ct) {
		this.time = ct.getLong("key");
	}
	
	public String getTime() {
		return new Date(time).toString();
	}

	@Override
	@SuppressWarnings({"unchecked"})
	public <T extends Component> T getCopy() {
		SomeComponent c = new SomeComponent();
		c.time = this.time;
		return (T) c;
	}

	@Override
	public ComponentKey<?> getKey() {
		return KEY;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof SomeComponent sc) return sc.time == this.time;
		return false;
	}

	@Override
	public void setHostObject(ComponentProvider o) {
		this.is = (ItemStack)(Object) o;
	}
}