package biom4st3r.libs.bioecs;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.minecraft.inventory.Inventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;

public class ClientLibInit implements ClientModInitializer {

    @Override
    public void onInitializeClient() {
        ClientPlayNetworking.registerGlobalReceiver(LibInit.SYNC_STACK, (client,handler,buf,sender) -> {
            Inventory inv = client.player.getInventory();
            int slot = buf.readInt();
            NbtCompound nbt = buf.readNbt();
            client.submit(() -> {
                inv.setStack(slot, ItemStack.fromNbt(nbt));
            });
        });
    }
    
}
