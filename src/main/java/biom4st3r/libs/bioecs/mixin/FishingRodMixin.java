package biom4st3r.libs.bioecs.mixin;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.FishingRodItem;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

import biom4st3r.libs.bioecs.SomeComponent;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({FishingRodItem.class})
public class FishingRodMixin {
    @Inject(
        at = @At("HEAD"), 
        method = "use", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private void bioecs$onUse(World world, PlayerEntity user, Hand hand, CallbackInfoReturnable<TypedActionResult<ItemStack>> ci) {
        user.sendMessage(Text.literal(SomeComponent.KEY.getComponent(user.getStackInHand(hand)).get().getTime()), false);
    }
}
