package biom4st3r.libs.bioecs.mixin.itemcomponents.mojanknotusingcopy;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import net.minecraft.block.AbstractCauldronBlock;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

@Mixin({AbstractCauldronBlock.class})
public class AbstractCauldronBlockMxn {
    /** Disabled in 1.19.3. Doesn't appear nessisary anymore */
    @Inject(
       at = @At(
          value="RETURN"),
       method = "onUse",
       cancellable = false,
       locals = LocalCapture.CAPTURE_FAILHARD)
    private void bioecs$cauldronBehavior$onUse(BlockState state, World world, BlockPos pos, 
            PlayerEntity player, Hand hand, BlockHitResult hit, 
            CallbackInfoReturnable<ActionResult> ci, ItemStack oldStack) {
        // CauldronBehavior reassigns stack in hand
        // ImplHelpers.copyInto(oldStack, player.getStackInHand(hand));
        
    }
}
