package biom4st3r.libs.bioecs.mixin.itemcomponents.mojanknotusingcopy;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import biom4st3r.libs.bioecs.ecs.api.ImplHelpers;
import net.minecraft.inventory.RecipeInputInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.BookCloningRecipe;
import net.minecraft.registry.DynamicRegistryManager;

@Mixin({BookCloningRecipe.class})
public class BookCloningRecipeMxn {
    @Inject(
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/item/ItemStack.setNbt(Lnet/minecraft/nbt/NbtCompound;)V", 
            ordinal = 0, 
            shift = Shift.AFTER), 
        method = "craft(Lnet/minecraft/inventory/RecipeInputInventory;Lnet/minecraft/registry/DynamicRegistryManager;)Lnet/minecraft/item/ItemStack;", 
        cancellable = false, 
        locals = LocalCapture.CAPTURE_FAILHARD)
    private void bioecs$bookCloning$mojangcopyStratiegery(RecipeInputInventory inventory, DynamicRegistryManager dynamicRegistryManager, CallbackInfoReturnable<ItemStack> ci, int i, ItemStack oldStack, ItemStack newStack) {
        ImplHelpers.copyInto(oldStack, newStack);
    }
}
