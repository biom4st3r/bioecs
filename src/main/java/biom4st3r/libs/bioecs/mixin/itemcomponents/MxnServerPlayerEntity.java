package biom4st3r.libs.bioecs.mixin.itemcomponents;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import biom4st3r.libs.bioecs.LibInit;
import biom4st3r.libs.bioecs.ecs.api.ComponentProvider;
import net.minecraft.item.ItemStack;
import net.minecraft.server.network.ServerPlayerEntity;

@Mixin({ServerPlayerEntity.class})
public abstract class MxnServerPlayerEntity extends MxnPlayerEntity {

    @Inject(at = @At("HEAD"), method = "playerTick")
    private void bioecs$syncStackTick(CallbackInfo ci) {
        for (int i = 0; i < this.getInventory().size(); i++) {
            final int slot = i;
            ComponentProvider stack = ComponentProvider.asComponentProvider(this.getInventory().getStack(i));
            stack.getLookup().forEachSyncedComponent(sync -> {
                if (sync.shouldSync()) {
                    LibInit.sendSlotUpdate((ServerPlayerEntity)(Object)this, slot, (ItemStack)(Object)stack);
                }
            });
        }
    }

}
