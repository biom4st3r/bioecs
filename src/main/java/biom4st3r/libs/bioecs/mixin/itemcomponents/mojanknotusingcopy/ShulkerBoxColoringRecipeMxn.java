package biom4st3r.libs.bioecs.mixin.itemcomponents.mojanknotusingcopy;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

import biom4st3r.libs.bioecs.ecs.api.ImplHelpers;
import net.minecraft.inventory.RecipeInputInventory;
import net.minecraft.item.DyeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.ShulkerBoxColoringRecipe;
import net.minecraft.registry.DynamicRegistryManager;

@Mixin({ShulkerBoxColoringRecipe.class})
public class ShulkerBoxColoringRecipeMxn {
    @Inject(
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/item/ItemStack.hasNbt()Z", 
            ordinal = 0, // Specificly near setTag
            shift = Shift.BEFORE), //net.minecraft.registry.DynamicRegistryManager
        method = "craft(Lnet/minecraft/inventory/RecipeInputInventory;Lnet/minecraft/registry/DynamicRegistryManager;)Lnet/minecraft/item/ItemStack;", 
        cancellable = false, 
        locals = LocalCapture.CAPTURE_FAILHARD)
    private void bioecs$recolorShulkerClone(RecipeInputInventory craftingInventory, DynamicRegistryManager dynamicRegistryManager, CallbackInfoReturnable<ItemStack> cir, ItemStack oldStack, DyeItem dyeItem, ItemStack newStack) {
        ImplHelpers.copyInto(oldStack, newStack);
    }
}
