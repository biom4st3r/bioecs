package biom4st3r.libs.bioecs.mixin.itemcomponents.mojanknotusingcopy;


import net.minecraft.entity.Entity;
import net.minecraft.entity.ItemSteerable;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.OnAStickItem;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

import biom4st3r.libs.bioecs.ecs.api.ImplHelpers;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({OnAStickItem.class})
public class OnAStickItemMxn {
    @Inject(
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/item/ItemStack.setNbt(Lnet/minecraft/nbt/NbtCompound;)V", 
            ordinal = 0, 
            shift = Shift.AFTER), 
        method = "use", 
        cancellable = false, 
        locals = LocalCapture.CAPTURE_FAILHARD)
    private void bioecs$convertToFishingRodClone(World world, PlayerEntity user, 
            Hand hand, CallbackInfoReturnable<TypedActionResult<ItemStack>> ci, ItemStack oldStack, 
            Entity entity, ItemSteerable item, ItemStack newStack) {
        ImplHelpers.copyInto(oldStack, newStack);
    }
}
