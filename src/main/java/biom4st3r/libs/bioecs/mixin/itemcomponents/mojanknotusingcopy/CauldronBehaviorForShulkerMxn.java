package biom4st3r.libs.bioecs.mixin.itemcomponents.mojanknotusingcopy;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.cauldron.CauldronBehavior;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import biom4st3r.libs.bioecs.ecs.api.ImplHelpers;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

/**
 * REEE can't mixin to interfaces
 */
@Mixin({CauldronBehavior.class})
public interface CauldronBehaviorForShulkerMxn {
    @Inject(
       at = @At(
          value="INVOKE",
          target="net/minecraft/item/ItemStack.setNbt(Lnet/minecraft/nbt/NbtCompound;)V",
          ordinal = 0,
          shift = Shift.AFTER),
       method = "method_32215",
       cancellable = false,
       locals = LocalCapture.CAPTURE_FAILHARD)
    private void cleanShulkerSynthetic(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, ItemStack oldStack, CallbackInfoReturnable<ActionResult> ci, Block block, ItemStack newStack) {
        ImplHelpers.copyInto(oldStack, newStack);
    }
}
