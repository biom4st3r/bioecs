package biom4st3r.libs.bioecs.mixin.itemcomponents;

import net.minecraft.item.Item;
import net.minecraft.item.ItemConvertible;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;

import biom4st3r.libs.bioecs.ecs.api.ComponentLookup;
import biom4st3r.libs.bioecs.ecs.api.ComponentProvider;
import biom4st3r.libs.bioecs.ecs.api.EntityDescription;
import biom4st3r.libs.bioecs.ecs.api.ImplHelpers;
import biom4st3r.libs.bioecs.ecs.impl.ComponentLookupImpl;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({ItemStack.class})
public abstract class MxnItemStack implements ComponentProvider {

    @Shadow @Final
    private Item item;
    @Shadow
    public abstract boolean isEmpty();
    // @Shadow
    // private boolean empty;

    @Inject(
        at = @At("TAIL"), 
        method = "<init>(Lnet/minecraft/item/ItemConvertible;I)V",
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE)
    private void bioecs$init(ItemConvertible item, int count, CallbackInfo ci) {
        if (this.item == null) return;
        bioecs$lookup = ComponentLookupImpl.newLookup(EntityDescription.as(this.item).getDescription(), this);
    }

    ComponentLookup bioecs$lookup = ComponentLookupImpl.EMPTY;
    @Override
    public ComponentLookup getLookup() {
        return bioecs$lookup;
    }

    @Inject(
        at = @At("TAIL"),
        method = "<init>(Lnet/minecraft/nbt/NbtCompound;)V"
    )
    private void bioecs$fromNbt(NbtCompound nbt, CallbackInfo ci) {
        bioecs$lookup = ComponentLookupImpl.newLookup(EntityDescription.as(this.item).getDescription(), this);
        this.getLookup().deserialize(nbt);
    }

    @Inject(
        at = @At(value = "RETURN", ordinal = 1),
        method = "copy"
    )
    private void bioecs$copy(CallbackInfoReturnable<ItemStack> ci) {
        ItemStack newStack = ci.getReturnValue();
        if(!newStack.isEmpty() && this.item == newStack.getItem()) {
            ImplHelpers.copyInto((ItemStack)(Object)this, newStack);
        }
    }

    @Inject(
        at = @At("TAIL"), 
        method = "writeNbt", 
        cancellable = false,
        locals = LocalCapture.NO_CAPTURE)
    private void bioecs$writeToNbt(NbtCompound tag, CallbackInfoReturnable<NbtCompound> ci) {
        this.getLookup().serialize(tag);
    }
    
    // @Inject(
    //     at = @At(value = "RETURN", ordinal = 3), 
    //     method = "isEqual", 
    //     cancellable = true,
    //     locals = LocalCapture.NO_CAPTURE)
    // private void bioecs$equals(ItemStack stack, CallbackInfoReturnable<Boolean> ci) {
    //     if(ci.getReturnValueZ()) {
    //         ci.setReturnValue(ImplHelpers.componentProviderCompare(this, stack));
    //     }
    // }

    @Inject(
        at = @At(value = "RETURN"), 
        method = "areEqual", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private static void bioecs$equals(ItemStack s0, ItemStack s1, CallbackInfoReturnable<Boolean> ci) {
        if(ci.getReturnValueZ()) {
            ci.setReturnValue(ImplHelpers.componentProviderCompare(s0, s1));
        }
    }

    @Inject(
        at = @At(value = "RETURN"), 
        method = "areEqual", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private static void bioecs$canCombine(ItemStack s0, ItemStack s1, CallbackInfoReturnable<Boolean> ci) {
        if(ci.getReturnValueZ()) {
            ci.setReturnValue(ImplHelpers.componentProviderCompare(s0, s1));
        }
    }

    /**
     * Vanilla Optimization?
     */
    @Inject(
        at = @At("HEAD"),
        method = "fromNbt",
        cancellable = true
    )
    private static void bioecs$optimization(NbtCompound nc, CallbackInfoReturnable<ItemStack> ci) {
        if(nc.isEmpty()) ci.setReturnValue(ItemStack.EMPTY);
    }

    @Inject(
        at = @At("RETURN"), 
        method = "areEqual", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private static void bioecs$areEqual(ItemStack left, ItemStack right, CallbackInfoReturnable<Boolean> ci) {
        if(ci.getReturnValueZ()) {
            ci.setReturnValue(ImplHelpers.componentProviderCompare(left, right));
        }
    } 

    // @Inject(
    //     at = @At("TAIL"), 
    //     method = "updateEmptyState", 
    //     cancellable = true,
    //     locals = LocalCapture.NO_CAPTURE)
    // private void bioecs$returnPooledComponent() {
    //     // if(this.empty) {
    //     //     this.getLookup().forEach(c-> {
    //     //         if(c.getKey() instanceof PoolingComponentKeyImpl key) {
                    
    //     //             key.returnComponent((PooledComponent)c);
    //     //         }
    //     //     });
    //     // }
    //     // this.bioecs$lookup = ComponentLookupImpl.EMPTY;
    // }

}
