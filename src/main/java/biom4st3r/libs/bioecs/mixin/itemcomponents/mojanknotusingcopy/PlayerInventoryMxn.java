package biom4st3r.libs.bioecs.mixin.itemcomponents.mojanknotusingcopy;

import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

import biom4st3r.libs.bioecs.ecs.api.ImplHelpers;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.At.Shift;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({PlayerInventory.class})
public class PlayerInventoryMxn {
    @Inject(
        at = @At(
            value = "INVOKE", 
            target = "net/minecraft/item/ItemStack.hasNbt()Z", 
            ordinal = 0, // Specificly near setTag
            shift = Shift.BEFORE), 
        method = "addStack(ILnet/minecraft/item/ItemStack;)I", 
        cancellable = false, 
        locals = LocalCapture.CAPTURE_FAILHARD)
    private void bioecs$addStackClone(int slot, ItemStack oldStack, CallbackInfoReturnable<Integer> ci, Item item, int count, ItemStack newStack) {
        ImplHelpers.copyInto(oldStack, newStack);
    }
}
