package biom4st3r.libs.bioecs.mixin.itemcomponents;

import net.minecraft.entity.ItemEntity;
import net.minecraft.item.ItemStack;

import biom4st3r.libs.bioecs.ecs.api.ImplHelpers;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import org.spongepowered.asm.mixin.injection.callback.LocalCapture;

@Mixin({ItemEntity.class})
public class MxnItemEntity {
    @Inject(
        at = @At(value = "RETURN", ordinal = 3), 
        method = "canMerge(Lnet/minecraft/item/ItemStack;Lnet/minecraft/item/ItemStack;)Z", 
        cancellable = true,
        locals = LocalCapture.NO_CAPTURE)
    private static void bioecs$canMerge(ItemStack stack0, ItemStack stack1, CallbackInfoReturnable<Boolean> ci) {
        if(ci.getReturnValueZ()) {
            ci.setReturnValue(ImplHelpers.componentProviderCompare(stack0, stack1));
        }
    }
}
