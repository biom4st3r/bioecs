package biom4st3r.libs.bioecs.ecs.impl;

import java.util.Arrays;
import java.util.Comparator;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Supplier;

import net.minecraft.nbt.NbtCompound;

import biom4st3r.libs.bioecs.ecs.api.Component;
import biom4st3r.libs.bioecs.ecs.api.ComponentKey;
import biom4st3r.libs.bioecs.ecs.api.ComponentLookup;
import biom4st3r.libs.bioecs.ecs.api.ComponentProvider;
import biom4st3r.libs.bioecs.ecs.api.SyncComponent;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ComponentLookupImpl implements ComponentLookup {
    public static final Comparator<ComponentKey<?>> SORTER = (a,b)-> a.getId() < b.getId()  ? -1 : a.getId() > b.getId() ? 1 : 0;
    public static final ComponentLookup EMPTY = new ComponentLookupImpl(new ComponentKeyImpl[0], null);

    public static ComponentLookup newLookup(ComponentKey<?>[] array, ComponentProvider hostObject) {
        if(array == null || array.length == 0) return EMPTY;
        return new ComponentLookupImpl(array, hostObject);
    }

    ComponentProvider hostObject;

    private ComponentLookupImpl(ComponentKey<?>[] array, ComponentProvider hostObject) {
        this.hostObject = hostObject;
        Arrays.sort(array, SORTER);

        this.ids = new int[array.length];
        this.components = new Component[array.length];
        this.proto_components = new SupplierWData[array.length];

        for (int i = 0; i < array.length; i++) {
            ids[i] = array[i].getId();
        }
    }

    @Override
    public void forEach(Consumer<Component> consumer) {
        for (int i = 0; i < this.components.length; i++) {
            consumer.accept(this.internal_fullInitAndGet(i, null));
        }
    }

    @Override // Synced components can't be cached and dirty
    public void forEachSyncedComponent(Consumer<SyncComponent> consumer) {
        for (int i = 0; i < this.components.length; i++) {
            Component c = this.components[i];
            if (c instanceof SyncComponent) consumer.accept((SyncComponent) c);
        }
    }

    /**
     * Lazy get
     * @param i
     * @return
     */ 
    @NotNull
    private Component internal_fullInitAndGet(int i, @Nullable ComponentKey<?> key) {
        if(key == null) key = ComponentKeyImpl.get(this.ids[i]);
        if(this.isCached(i)) {
            return internal_evalProtoAndGet(i);
        } else if(components[i] == null) {
            this.components[i] = key.getNew(hostObject);
            this.components[i].setHostObject(hostObject);
        }
        return this.components[i];
    }

    @Nullable
    private Component internal_evalProtoAndGet(int i) {
        if(this.isCached(i)) {
            this.evaluateProto(i);
            this.components[i].setHostObject(hostObject);
            this.proto_components[i] = null;
        }
        return this.components[i];
    }

    private boolean isCached(int i) {
        return this.proto_components[i] != null;
    }

    private Component evaluateProto(int i) {
        return this.components[i] = this.proto_components[i].get();
    }

    ReentrantLock LOCK = new ReentrantLock();

    /**
     * Stored the IDS of the componentKeys of the components
     */
    private final int[] ids;

    /**
     * Stores components that have been access at least 1 time
     */
    private final Component[] components;

    /**
     * Lazy. Reduces calls to copy by storing a direct reference to ::copy to be evaluated when the new copy is accessed at least once.
     */
    private final SupplierWData[] proto_components;

    private boolean isSyncSupplier(int i) {
        if (components[i] != null) {
            return components[i] instanceof SyncComponent;
        } else if (proto_components[i] == null) {
            return false;
        } else {
            return proto_components[i].isSyncComponet();
        }
    }

    private static class SupplierWData implements Supplier<Component> {

        private boolean isSync;
        private Component component;
        public SupplierWData(Component component) {
            this.component = component;
            this.isSync = (component instanceof SyncComponent);
        }

        @Override
        public Component get() {
            return component.getCopy();
        }

        public boolean isSyncComponet() {
            return this.isSync;
        }
    }

    @SuppressWarnings({"unchecked"})
    public <T extends Component> T getComponent(ComponentKey<T> key) {
        // normal lookup: FFFFMMMMSSSS
        //               fSSMMFFFFMMSS
        if(this.ids.length == 0) return null;

        LOCK.lock();
        if(this.ids[0] == key.getId()) {
            T t = (T) this.internal_fullInitAndGet(0, key);
            LOCK.unlock();
            return t;
        }

        int mid = ids[(ids.length-1)/2];
        for(int i = 0; i+mid < ids.length; i++) { // MID->
            int keyId = ids[i+mid];
            if(keyId == key.getId()) {
                T t = (T) this.internal_fullInitAndGet(i+mid, key);
                LOCK.unlock();
                return t;
            }
            else if(keyId > key.getId()) break;
        }
        for(int i = -1; i + mid >= 0; i--) { // <-MID
            int keyId = ids[i+mid];
            if(keyId == key.getId()) {
                T t = (T) this.internal_fullInitAndGet(i+mid, key);
                LOCK.unlock();
                return t;
            }
            else if(keyId < key.getId()) break;
        }
        LOCK.unlock();
        return null;
    }

    @Override
    public int size() {
        return ids.length;
    }

    private boolean isEmpty() {
        return this == EMPTY ? true : this.size() == 0;
    }

    @Override
    public void copyIntoNewLookup(ComponentLookup lookup) {
        if(isEmpty()) return;
        if(lookup.size() != this.size()) throw new RuntimeException();
        if(lookup instanceof ComponentLookupImpl impl) {
            for (int i = 0; i < components.length; i++) {
                LOCK.lock();
                if(this.isCached(i)) {
                    impl.proto_components[i] = this.proto_components[i];
                } else if(this.components[i] != null) {
                    Component c = this.components[i];
                    impl.proto_components[i] = new SupplierWData(c);
                }
                LOCK.unlock();
            }
            return;
        }
        throw new RuntimeException();
    }
    public static final String KEY = "bio$ecs";

    @Override
    public void serialize(NbtCompound tag) {
        if(isEmpty()) return;
        NbtCompound root = new NbtCompound();
        LOCK.lock();
        for (int i = 0; i < components.length; i++) {
            Component c = this.internal_evalProtoAndGet(i); // compute it if you got it.
            
            if(c == null) continue; // else just move on

            NbtCompound nc = new NbtCompound();
            c.serialize(nc);
            root.put(c.getKey().getIdentifier(), nc);
        }
        LOCK.unlock();
        tag.put(KEY, root);
    }

    @Override
    public void deserialize(NbtCompound tag) {
        if(isEmpty()) return;
        NbtCompound root = tag.getCompound(KEY);
        for (int i = 0; i < components.length; i++) {
            ComponentKey<?> key = ComponentKeyImpl.LIST.get(this.ids[i]);
            NbtCompound instance = root.getCompound(key.getIdentifier());
            if(!instance.isEmpty()) this.internal_fullInitAndGet(i, key).deserialize(instance);
        }
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ComponentLookupImpl impl) {
            if(this.isEmpty() || impl.isEmpty()) return true;
            boolean areEqual = this.components.length == impl.components.length;
            LOCK.lock();
            for (int i = 0; i < this.components.length && areEqual; i++) {
                // Is there a way to do this without evaluating the cache?
                // If it's still a proto SyncComponents shouldn't need to sync
                if(this.proto_components[i] != null && this.proto_components[i] == impl.proto_components[i]) {
                    areEqual &= true;
                    continue;
                }

                if (this.isSyncSupplier(i)) {
                    areEqual &= true;
                    continue;
                }

                Object o = this.internal_evalProtoAndGet(i);
                Object l = impl.internal_evalProtoAndGet(i);
                if (o instanceof SyncComponent sc) {
                    // return !sc.shouldSync();
                    areEqual &= true;
                } else if (o == null) {
                    areEqual &= o == l;
                } else if (l == null) {
                    areEqual = false; // o isn't null l is null
                } else {
                    areEqual &= o.equals(l);
                }
            }
            LOCK.unlock();
            return areEqual;
        }
        return false;
    }

}
