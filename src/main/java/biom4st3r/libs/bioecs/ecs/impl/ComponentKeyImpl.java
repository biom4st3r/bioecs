package biom4st3r.libs.bioecs.ecs.impl;

import java.util.List;
import java.util.Optional;

import biom4st3r.libs.bioecs.ecs.api.Component;
import biom4st3r.libs.bioecs.ecs.api.ComponentKey;
import biom4st3r.libs.bioecs.ecs.api.ComponentProvider;
import com.google.common.base.Preconditions;
import com.google.common.base.Supplier;
import com.google.common.collect.Lists;

public class ComponentKeyImpl<T extends Component> implements ComponentKey<T> {

    static List<ComponentKey<?>> LIST = Lists.newArrayList();

    public static ComponentKeyImpl<?> get(int i) {
        return (ComponentKeyImpl<?>) LIST.get(i);
    }

    static int GLOBAL_ID = 0;
    final int id;
    final String identifier;

    ComponentKeyImpl(String identifier, Supplier<T> getNew, Class<T> clazz) {
        this.identifier = identifier;
        this.getNew = getNew;
        this.clazz = clazz;
        this.id = GLOBAL_ID;
        GLOBAL_ID++;
    }

    final Supplier<T> getNew;
    final Class<?> clazz;

    public String getIdentifier() {
        return this.identifier;
    }

    public int getId() {
        return this.id;
    }

    @SuppressWarnings({"unchecked","rawtypes"})
    public Optional<T> getComponent(ComponentProvider provider) {
        Object o = provider.getLookup().getComponent(this);
        if(o == null) return Optional.empty();
        return (Optional) Optional.of(o);
    }

    public T getNew(ComponentProvider hostObject) {
        T t = getNew.get();
        t.setHostObject(hostObject);
        return t;
    }

    public static <T extends Component> ComponentKeyImpl<T> of(Class<T> clazz,Supplier<T> getNew,String identifier) {
        Preconditions.checkNotNull(clazz);
        Preconditions.checkNotNull(getNew);
        Preconditions.checkNotNull(getNew.get());
        Preconditions.checkNotNull(identifier);

        ComponentKeyImpl<T> key = new ComponentKeyImpl<>(identifier, getNew, clazz);

        if(LIST.contains(key)) throw new IllegalArgumentException("Duplicate Entry");

        LIST.add(key);

        return key;
    }

    @Override
    public int hashCode() {
        return this.clazz.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ComponentKeyImpl<?> key) {
            return this.clazz == key.clazz;
        }
        return false;
    }
}