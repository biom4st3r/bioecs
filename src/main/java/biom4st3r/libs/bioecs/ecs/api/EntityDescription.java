package biom4st3r.libs.bioecs.ecs.api;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

/**
 * Describes the makeup(Components) on the Entity
 */
@AvailableSince("0.1.0")
public interface EntityDescription {
    ComponentKey<?>[] getDescription();
    static EntityDescription as(Object o) {
        return (EntityDescription) o;
    }
} 
