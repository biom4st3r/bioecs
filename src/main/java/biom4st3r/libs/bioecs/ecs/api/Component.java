package biom4st3r.libs.bioecs.ecs.api;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

import net.minecraft.nbt.NbtCompound;

@AvailableSince("0.1.0")
public interface Component {
    void serialize(NbtCompound ct);
    void deserialize(NbtCompound ct);
    <T extends Component> T getCopy();
    ComponentKey<?> getKey();
    /**
     * When a components to added to its `Entity` it is provided a reference to its holder.
     * @param o
     */
    void setHostObject(ComponentProvider o);

    @Override
    boolean equals(Object o);
}