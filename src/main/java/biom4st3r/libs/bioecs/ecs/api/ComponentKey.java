package biom4st3r.libs.bioecs.ecs.api;

import java.util.Optional;

import org.apache.commons.lang3.NotImplementedException;
import org.jetbrains.annotations.ApiStatus.AvailableSince;

@AvailableSince("0.1.0")
public interface ComponentKey<T extends Component> {

    /**
     * @throws NotImplementedException if not supported 
     * @return
     */
    int getId();
    String getIdentifier();
    /**
     * generates a new Component.
     * @param hostObject
     * @return
     */
    T getNew(ComponentProvider hostObject);
    Optional<T> getComponent(ComponentProvider provider);
    default Optional<T> getComponent(Object provider) {
        return this.getComponent((ComponentProvider) provider);
    }
}
