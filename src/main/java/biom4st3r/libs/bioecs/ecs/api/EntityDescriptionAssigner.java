package biom4st3r.libs.bioecs.ecs.api;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

@AvailableSince("0.1.0")
public interface EntityDescriptionAssigner {
    public void set(ComponentKey<?>[] keys);
    public void add(ComponentKey<?> key);
}
