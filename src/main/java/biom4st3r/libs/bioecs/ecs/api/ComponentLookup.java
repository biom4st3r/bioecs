package biom4st3r.libs.bioecs.ecs.api;

import java.util.function.Consumer;

import org.jetbrains.annotations.ApiStatus.AvailableSince;

import net.minecraft.nbt.NbtCompound;

/**
 * Holds all components on the entity
 */
@AvailableSince("0.1.0")
public interface ComponentLookup {
    public <T extends Component> T getComponent(ComponentKey<T> key);
    public int size();
    public void copyIntoNewLookup(ComponentLookup lookup);
    public void forEach(Consumer<Component> component);
    public void serialize(NbtCompound nc);
    public void deserialize(NbtCompound nc);

    @AvailableSince("0.1.5")
    public void forEachSyncedComponent(Consumer<SyncComponent> consumer);
    @Override
    public boolean equals(Object object);
}
